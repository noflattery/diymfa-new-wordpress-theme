<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-3of3 d-7of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<div class="error-left hentry cf m-all t-2of3 d-4of7" style="max-width: 650px;">

								<header class="article-header entry-header">

									<h1 class="entry-title single-title"><?php the_field('header_copy', 'options'); ?></h1>

								</header>

								<p><?php the_field('first_text_area', 'options'); ?></p>
								<?php include (TEMPLATEPATH . '/includes/section-join-404.php' ); ?>	

							</div>

					
							<div class="error-right hentry cf m-all t-1of3 d-3of7">

								<?php 

								$image = get_field('error_image', 'options');

									if($image): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

									<?php endif; 
								?>

								<p><?php the_field('second_text_area', 'options'); ?></p>										
									<!-- <p><?php _e( 'This is the 404.php template.', 'bonestheme' ); ?></p> -->

							</div>

						</div>

					</main>

				</div>

			</div>

<?php get_footer(); ?>
