<div id="rightbar" class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">

<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Right Sidebar')) ?>
	<?php get_search_form(); ?>

	<?php if( have_rows('right_sidebar_custom_options', 'options') ): ?>
 
	    <?php while( have_rows('right_sidebar_custom_options', 'options') ): the_row(); 

	    	$title = get_sub_field('title_new');
	    	$content = get_sub_field('content_new');
	    	$bgcolor = get_sub_field('color');

	    ?>
	 			
	 		<div class="custom-sidebar box" style="background-color: <?php echo $bgcolor; ?>;">
	 			<h2><?php echo $title; ?></h2>
	 			<?php echo $content; ?>
	 		</div>

	    <?php endwhile; ?>
 
	<?php endif; ?>

	<?php if(is_single()) {
		related_posts();
	}
	?>
</div>