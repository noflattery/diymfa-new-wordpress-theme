<?php get_header(); ?>

<div id="content">

	<div id="inner-content" class="wrap cf">

		<main id="main" class="m-all d-5of7 cf wider-5of7" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

				<div class="entry-thumbnail">
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
						<?php 
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail(300, 300);
						} 
						?>
					</a>
				</div>


				<section class="entry-content cf m-all">
					<header class="article-header">

					<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
					<p class="byline entry-meta vcard">
                    <?php 
							/* the time the post was published */
						echo '<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>'; 
							/* formerly contained the author content*/
					?>
					</p>

				</header>
			

					<?php the_excerpt('Read more on "'.the_title('', '', false).'" &raquo;'); ?>

					<footer class="article-footer cf">
					<p class="article-category">
					<?php $category = get_the_category(); 
		echo '<a href="'.get_category_link($category[0]->cat_ID).'" class="icon '.$category[0]->slug.'"><i><span></span></i>'.$category[0]->name.'</a>'; ?>
						<!-- <a href=""><img src="/imwe" /></a> -->
					</p><!--.article-category-->

				</footer>
				</section>


			</article>

			<?php endwhile; ?>

					<?php bones_page_navi(); ?>

			<?php else : ?>

					<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

			<?php endif; ?>


		</main>

		<?php get_sidebar(); ?>

	</div>

</div>


<?php get_footer(); ?>
