<?php 
// Create new join phrase based on content type
if (is_page( 6 )) { // Writer Igniter
	$text_select = "app";
} elseif (is_page()) {
	$text_select = "info";
} else {
	$text_select = "article";
} ?>

<section class="join">
	
	<div class="m-all t-3of3 d-3of3">
		<script src="https://app.convertkit.com/assets/CKJS4.js?v=21"></script>

		<div class="ck_form ck_minimal">
		    <div class="ck_form_fields">
			    <div id="ck_success_msg"  style="display:none;">
			      <p>Success! Now check your email to confirm your subscription.</p>
			    </div>
			    <!--  Form starts here  -->
			    <form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/24205/subscribe" data-remote="true">
					<input type="hidden" value="{&quot;form_style&quot;:&quot;minimal&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
					<input type="hidden" name="id" value="24205" id="landing_page_id">
					<div class="ck_errorArea">
						<div id="ck_error_msg" style="display:none">
						  <p>There was an error submitting your subscription. Please try again.</p>
						</div>
					</div>
					<input type="email" name="email" class="ck_email_address text" id="ck_emailField" placeholder="" required>
					<input id="ck_subscribe_button" name="submit" class="submit button subscribe_button ck_subscribe_button btn fields" type="submit" value="JOIN TODAY" tabindex="501" />
					
					<div class="join-badge" title="Join our mailing list for email updates and a FREE STARTER KIT.">
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/free-starter-kit.svg" />
					</div>

			    </form>

		    </div>
		</div>
	</div>

</section>