jQuery(document).ready(function($){

	// Show Cart
	$('.wpsc_buy_button').click(function(){
		$('.widget_wpsc_shopping_cart').slideDown();
	});
	$('.emptycart').click(function(){
		$('.widget_wpsc_shopping_cart').slideUp();
	});

	// Colorbox for Inline Video with automatic stop on close
	$(".colorbox-link").colorbox({
		inline:true,
		onClosed:function(){ 
		$('video').each(function(){this.player.pause()})
		}
	});

	// Open External Links in New Window
	$('.social a').each(function() {
	var a = new RegExp('/' + window.location.host + '/');
    if(!a.test(this.href)) {
		$(this).click(function(event) {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
       	});
	   }
	});

	// Arrows
	$('aside h2, .yarpp-related h2').append('<i class="ss-icon">&#x25BE;</i>');

	//Remove Empty Paragraphs
	$( 'p:empty' ).remove();
	$('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
	});
	
	
	var url = location.pathname; // Only load on Writer Igniter
	if (url.indexOf("writer-igniter") >= 0 && url.indexOf("product") < 0){
		// Writer Igniter
		$('.slot').jSlots({  
			spinner : '#playBtn',  
			number : 1,  
			easing: 'easeOutSine',
			time:4000,
			loops:4
		});  	
	}
	
	// Slots
	var i = 1;
	$('div.jSlots-wrapper').each(function(i){
		var num = i + 1;
		$(this).addClass('slot-' + num )
	});


	//Testimonials
	$('#fade').cycle({
		slideResize: true,
		containerResize: true,	
	});
	
	

});