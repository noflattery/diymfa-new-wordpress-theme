/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *	// update the viewport, in case the window size has changed
 *	viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

  $('#menu-main-menu').slicknav({
    label: 'Menu',
    duration: 200
  });

  $('#menu-main-menu').append('<li class="social"><div class="social-container"><span>follow:</span><a href="https://twitter.com/DIYMFA" alt="Follow DIY MFA on Twitter" title="Follow DIY MFA on Twitter" class="icon ss-social ss-twitter"><img src="/wp-content/themes/diymfa/library/images/twitter.svg" /></a><a href="https://www.facebook.com/DIYMFA" alt="Follow DIY MFA on Facebook" title="Follow DIY MFA on Facebook" class="icon ss-social ss-facebook"> <img src="/wp-content/themes/diymfa/library/images/facebook.svg" /></a></div></li>');
  // add join to top, search and social to bottom of menu
  $('.slicknav_nav').prepend('<li class="menu-item join-menu-item"><a href="/join"><div class="join-link-mobile">Join</div></a></li>');
  $('.slicknav_nav').append('<li class="menu-item search-menu-item"><i class="fa fa-search"></i> Search</li>');
  $('.slicknav_nav').append('<li class="social"><span>follow:</span><a href="https://www.facebook.com/DIYMFA" alt="Follow DIY MFA on Facebook" title="Follow DIY MFA on Facebook" class="icon ss-social ss-facebook"><img src="/wp-content/themes/diymfa/library/images/facebook.svg" /></a><a href="https://twitter.com/DIYMFA" alt="Follow DIY MFA on Twitter" title="Follow DIY MFA on Twitter" class="icon ss-social ss-twitter"><img src="/wp-content/themes/diymfa/library/images/twitter.svg" /></a></li>');

  $('.slicknav_nav').append('<div class="menu-more">Scroll <i class="fa fa-arrow-down"></i></div>');
  /* mobile search */

  $('.search-menu-item').on('click', function(){
    $('#search-overlay').fadeIn();
  });

  $('.overlay-close').on('click', function(){
    $('#search-overlay').fadeOut();
  });

  $('.slicknav_collapsed').on('click', function(){
    $('.menu-more').fadeIn();
  });

  

  // check if logo is on screen to show menu arrow

  $.fn.is_on_screen = function(){
       
      var win = $(window);
       
      var viewport = {
          top : win.scrollTop(),
          left : win.scrollLeft()
      };
      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();
       
      var bounds = this.offset();
      bounds.right = bounds.left + this.outerWidth();
      bounds.bottom = bounds.top + this.outerHeight();
       
      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
       
  };

  if( $('#container').length > 0 ) { // if target element exists in DOM
    if( $('#container').is_on_screen() ) { // if target element is visible on screen after DOM loaded
          $('.menu-more').hide();
    } else {
          $('.menu-more').show();
    }
  }
  $(window).scroll(function(){ // bind window scroll event
    if( $('#container').length > 0 ) { // if target element exists in DOM
      if( $('#container').is_on_screen() ) { 
        $('.menu-more').hide();
      } else {
        $('.menu-more').show();
      }
    }
  });

  if($(window).width() < 768)
  {
     $('.category-podcast .entry-content iframe').appendTo('.category-single-post');
  }

  
  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();


   /*
   Ported over from original HTML5 Sprucetree theme
    */
    // Arrows on sidebar titles
  $('.custom-sidebar h2').append('<i class="ss-icon">&#x25BE;</i>');


  /* WooCommerce and bones scripts */

  $('.woocommerce ul.products li.product').addClass('woo-d-1of3');
  $('.header-cart-display').appendTo('#menu-main-menu').show();
  $('.mobile-cart-display').appendTo('.slicknav_nav').show();


}); /* end of as page load scripts */
