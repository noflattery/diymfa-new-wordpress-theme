<?php
/*
 Template Name: New Homepage
*/
?>

<?php get_header(); ?>

<div id="intro" class="wrap m-all">

	<div class="left-intro t-2of5 d-2of5">
			<h2>write<span> More</span></h2>
			<h2>write<span> Better</span></h2>
			<h2>write<span> Smarter</span></h2>
			
			<?php 				
			// Featured Video Large 
			//$link1 = get_field('video_url'); 
			//$var = apply_filters('the_content', '[embed width="530" height="345"]' . $link1 . '[/embed]');
			//echo $var; 
			?>
	</div>
		
	<div class="right-intro t-3of5 d-3of5">
		<?php the_field('intro_text', 'options'); ?>
		<img src="<?php the_field('intro_image', 'options'); ?>" />
	</div>	

</div>
<div class="signup wrap cf">

		<div class="left-text">
			<a class="biolink" href="/contact/gabriela-pereira">Meet our founder &raquo;
			</a>
			
			<h3><a title="Join DIY MFA" href="http://diymfa.com/join">Sign up</a> for email updates and get a free DIY MFA Starter Kit.
			</h3>
			<p>
				<a href="http://diymfa.com/about" title="Learn more about how DIY MFA works">Learn more about how DIY MFA works » </a>
			</p>
			<!-- <h3>
			<?php if( get_field('join_prompt') ): ?>
			<p><?php the_field('join_prompt'); ?></p>
			<?php endif; ?>

			</h3> -->
			<!-- <p>
				<a href="<?php the_field('learn_more_url'); ?>" title="<?php the_field('learn_more_link_text'); ?>"><?php the_field('learn_more_link_text'); ?> &raquo; </a>
			</p> -->
		</div>

		<div class="right-text">
			<script src="https://app.convertkit.com/assets/CKJS4.js?v=21"></script>
			<!--  Form starts here  -->
		    <form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/24205/subscribe" data-remote="true">
				<input type="hidden" value="{&quot;form_style&quot;:&quot;minimal&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
				<input type="hidden" name="id" value="24205" id="landing_page_id">
				<div class="ck_errorArea">
					<div id="ck_error_msg" style="display:none">
					  <p>There was an error submitting your subscription. Please try again.</p>
					</div>
				</div>
				<input type="email" name="email" class="ck_email_address text" id="ck_emailField" placeholder="" required>
				<input id="ck_subscribe_button" name="submit" class="submit button subscribe_button ck_subscribe_button btn fields join-today" type="submit" value="JOIN TODAY" tabindex="501" />
		    </form>
		</div>
		
	</div>

<div id="content">

	<div id="inner-content" class="wrap cf">

		<main id="main" class="m-all t-2of3 d-5of7 cf wider-5of7" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			
			<?php $the_query = new WP_Query(array('posts_per_page' => 5)); ?>
			<?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
				<div class="entry-thumbnail m-all d-1of4">
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
						<?php 
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail(300, 300);
						} 
						?>
					</a>
				</div>

				<section class="entry-content cf m-all">
					<header class="article-header">
						<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
							<p class="byline entry-meta vcard">
		                    <?php
		       				    echo '<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>';
		       				?>
							</p>
					</header>
			
					<?php the_excerpt('Read more on "'.the_title('', '', false).'" &raquo;'); ?>

					<footer class="article-footer cf">
						<p class="article-category">
						<?php $category = get_the_category(); 
						echo '<a href="'.get_category_link($category[0]->cat_ID).'" class="icon '.$category[0]->slug.'"><i><span></span></i>'.$category[0]->name.'</a>'; ?>
							<!-- <a href=""><img src="/imwe" /></a> -->
						</p><!--.article-category-->
					</footer>
				</section>
			</article>

			<?php endwhile; ?>
				<a href="/articles" class="multibutton more-articles"><span>More Articles <i class="ss-icon">▹</i></span></a>

			<?php else : ?>

					<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

			<?php endif; ?>


		</main>

		<?php get_sidebar(); ?>

	</div>

</div>


<?php get_footer(); ?>
