			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<div class="wrap">		
						<div id="footer-container">		
							<a href="<?php bloginfo('url'); ?>">DIY MFA</a> &copy;<?php echo date('Y'); ?> All Rights Reserved.	DIY MFA is a registered trademark of DIY MFA L.L.C.			

							<?php if (is_front_page()) { ?>						
								<?php include (TEMPLATEPATH . '/social.php' ); ?>			
							<?php } ?>

							<ul class="footernav">
								<li><a href="/terms-conditions">Terms &amp; Conditions</a><span class="divider-footer"> | </span></li> 
								<li><a href="/privacy">Privacy Policy</a><span class="divider-footer"> | </span></li> 
								<li><a href="/disclosure">Disclosure</a></li>
							</ul>
											
							<div class="spruce">Designed by <a href="http://www.spruceinteractive.com">SPRUCE interactive</a>. Theme built by <a href="http://www.nathanalexanderthompson.com">NT</a></div>			
						</div>
					</div>

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>

		<?php if (is_page( 'writer-igniter' )) { ?>						
			<script src="<?php bloginfo('template_directory');?>/library/js/libs/jquery.jSlots.min.js" charset="utf-8"></script> 	
			<script src="<?php bloginfo('template_directory');?>/library/js/libs/jquery.easing.1.3.js" charset="utf-8"></script> 		
		<?php } ?>
		
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery.colorbox-min.js"></script>	

		<script src="<?php echo get_template_directory_uri(); ?>/library/js/functions.js"></script>		
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery.slicknav.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery.cycle.all.js"></script>

		<script src="https://app.convertkit.com/landing_pages/39155.js"></script>

		<?php wp_footer(); ?>

			<!--Typekit-->
			<script src="https://use.typekit.net/cbe6fcu.js"></script>
			<script>try{Typekit.load({ async: true });}catch(e){}</script>

		<script>

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-15984775-6']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	</body>

</html> <!-- end of site. what a ride! -->
