<?php /*
Default Template
*/
		
echo '<div class="fourth box"><h2>Related Articles</h2>';
	if ( have_posts() ) : while ( have_posts() ) : the_post();
	echo '<h4><a href="'. get_permalink().'" title="' . get_the_title() . '">' . get_the_title() . '</a></h4>';
	echo '<time class="date">'.get_the_date().'</time>';
	echo '<a class="readit" href="'. get_permalink().'" title="Read Article">Read Article &raquo;</a>';	
	endwhile; endif; 
	echo '<a href="/articles" class="icon"></i>More Articles &raquo;</a>'; 
echo '</div>';

?>