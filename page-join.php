<?php
/*
 Template Name: Join
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<div class="join-intro">
							<div class="m-all d-1of2 join-intro-left">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/StarterKit.png" />
							</div>
							<div class="m-all-d-1of2 join-intro-right">
								<h1><?php the_field('headline'); ?></h1>

								<?php the_field('benefits_list'); ?>
								<section class="join">
									<script src="https://app.convertkit.com/assets/CKJS4.js?v=21"></script>
									<div class="ck_form ck_minimal">
									    <div class="ck_form_fields">
										    <div id="ck_success_msg"  style="display:none;">
										      <p>Success! Now check your email to confirm your subscription.</p>
										    </div>
										    <!--  Form starts here  -->
										    <form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/24205/subscribe" data-remote="true">
												<input type="hidden" value="{&quot;form_style&quot;:&quot;minimal&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
												<input type="hidden" name="id" value="24205" id="landing_page_id">
												<div class="ck_errorArea">
													<div id="ck_error_msg" style="display:none">
													  <p>There was an error submitting your subscription. Please try again.</p>
													</div>
												</div>
												<input type="email" name="email" class="ck_email_address text" id="ck_emailField" placeholder="" required>
												<input id="ck_subscribe_button" name="submit" class="submit button subscribe_button ck_subscribe_button btn fields" type="submit" value="JOIN TODAY" tabindex="501" />
										    </form>
									    </div>
									</div>
								</section>
							</div>
						</div>

							<div class="join-supportive">
									<div class="color-divider" style="margin-top: 30px;"></div>
									<ul>
										<li><h3>write more.</h3><br><div class="custom-write-text"><?php the_field('write_more_text'); ?></div></li>
										<li><h3>write better.</h3><br><div class="custom-write-text"><?php the_field('write_better_text'); ?></div></li>
										<li><h3>write smarter.</h3><br><div class="custom-write-text"><?php the_field('write_smarter_text'); ?></div></li>
									</ul>
									<div class="color-divider"></div>	
								<section id="testimonials">
									<h2>Testimonials<i class="ss-icon">▾</i></h2>
										
										<div id="fade">
											<?php  
											// or the_repeater_field + the_sub_field
											if(get_field('testimonials_repeater')): ?>   
											<?php while(the_repeater_field('testimonials_repeater')): ?>
											    
											    <article>
												    <div class="m-all d-1of4 testimonial-image">
														<?php $image = wp_get_attachment_image_src(get_sub_field('photo')); ?>   
															<img src="<?php echo $image[0]; ?>" />
													</div>

													<div class="m-all d-3of4 testimonial-text">
														<p style="width: auto;">&ldquo;<?php the_sub_field('testimonial_text'); ?>&rdquo;</p>
														<p class="author"><?php the_sub_field('testimonial_writer'); ?>, <?php the_sub_field('title_of_position'); ?></p>
													</div>
											 	</article>
											<?php endwhile; ?><?php endif; ?>
											
										</div>
										<div class="clear"></div>
								</section>
							</div>

				</div>
			</div>


<?php get_footer(); ?>
