<div class="meta">		
<time class="date"><?php the_date('M d, Y', '', ''); ?></time>	<span class="author">by <?php the_field('author'); ?></span>	
<?php 		
	$category = get_the_category(); 		
	echo '<a href="'.get_category_link($category[0]->cat_ID).'" class="icon '.$category[0]->slug.'"><i><span></span></i>published in '.$category[0]->name.'</a>'; 	?>
</div>